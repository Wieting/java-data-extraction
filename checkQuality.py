import sys
import operator

f = open(sys.argv[1])
lines = f.readlines()
d={}

for i in lines:
    if("|||" in i):
        i=i.split("|||")
        p1=i[0].strip()
        p2=i[1].strip()
        if p1 in d:
            d[p1]=d[p1]+1
        else:
            d[p1]=1
        if p2 in d:
            d[p2]=d[p2]+1
        else:
            d[p2]=1

sorted_d = sorted(d.items(), key=operator.itemgetter(1))

for i in sorted_d:
    print i