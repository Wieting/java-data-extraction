package base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import makeData.LD;
import makeData.makeData2.Stuff;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class getVerbNounBase {

	public static void main(String[] args) {
		// Initialize the tagger
		args = new String[1];
		args[0] = "verb-noun-base-xl.txt";
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		HashSet<String> examples = new HashSet<String>();
		HashSet<String> done = new HashSet<String>();

		HashSet<String> vocab = new HashSet<String>();
		File vfile = new File("/shared/shelley/wieting2/matlab/paraphrase_RNN/bigrams/make_data/vocab.skip25w5-wiki.en");
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(vfile);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		int count = 0;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				vocab.add(line.trim());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//getTags("; run the following command .",pipeline,"dobj",vocab);
		//System.exit(1);

		try {
			File file = new File("/shared/shelley/wieting2/matlab/paraphrase_RNN/bigrams/make_data/ppdb/ppdb-1.0-xl-phrasal_scored");
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			count = 0;
			while ((line = bufferedReader.readLine()) != null) {
				count ++;
				if(count % 10000 == 0) {
					System.out.println("read "+count+" lines and found "+examples.size() +" examples.");
					//break;
				}
				String[] arr = line.split("\\|\\|\\|");
				//System.out.println(arr[arr.length - 1]);
				String p1 = arr[2];
				String p2 = arr[3];
				String al = arr[arr.length - 1];
				double score = Double.parseDouble(arr[0]);
				String[] phrase1 = p1.trim().split("\\s");
				String[] phrase2 = p2.trim().split("\\s");
				Stuff s1 = getTags(p1,pipeline, "V","N",vocab);
				ArrayList<String> pos1 = s1.tags;
				ArrayList<Integer> ints1 = s1.ints;

				Stuff s2 = getTags(p2,pipeline, "V","N",vocab);
				ArrayList<String> pos2 = s2.tags;
				ArrayList<Integer> ints2 = s2.ints;
				if(pos1.size()== 0 || pos2.size() == 0)
					continue;

				String al1 = ints1.get(0)+"-"+ints2.get(0);
				String al2 = ints1.get(1)+"-"+ints2.get(1);

				if(!al.contains(al1) || !al.contains(al2))
					continue;

				p1 = pos1.get(0)+" "+pos1.get(1);
				p2 = pos2.get(0)+" "+pos2.get(1);

				if(LD.ld(p1,p2)<=1)
					continue;


				if(p1.contains("-lrb-") || p1.contains("-rrb-") || p2.contains("-lrb") || p2.contains("-rrb"))
					continue;

				if(p1.compareTo(p1) < 0) {
					if(!done.contains(p1+"|||"+p2)) {
						examples.add(p1+"|||"+p2+"|||"+score);
						done.add(p1+"|||"+p2);
					}
				}
				else {
					if(!done.contains(p1+"|||"+p2)) {
						examples.add(p2+"|||"+p1+"|||"+score);
						done.add(p1+"|||"+p2);
					}
				}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for(String s: examples) {
			System.out.println(s);
		}

		System.out.println(examples.size());
		writeExamples(examples, args[0]);
	}

	private static Stuff getTags(String p1, StanfordCoreNLP pipeline, String type1, String type2, HashSet<String> vocab) {
		Annotation document = new Annotation(p1);

		// run all Annotators on this text
		pipeline.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		ArrayList<String> tags = new ArrayList<String>();
		ArrayList<Integer> ints = new ArrayList<Integer>();

		for(CoreMap sentence: sentences) {

			List<CoreLabel> lis = sentence.get(TokensAnnotation.class);
			int t=0;
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {

				String word = token.get(TextAnnotation.class);
				String pos = token.get(PartOfSpeechAnnotation.class);
				String lem = token.get(LemmaAnnotation.class);
				if(pos.startsWith(type1)) {
					int tt=0;
					for (CoreLabel token2: sentence.get(TokensAnnotation.class)) {
						String word2 = token2.get(TextAnnotation.class);
						String pos2 = token2.get(PartOfSpeechAnnotation.class);
						String lem2 = token2.get(LemmaAnnotation.class);
						if(pos2.startsWith(type2)) {
							if(tt > t) {
								if(!vocab.contains(word) || !vocab.contains(word2))
									continue;
								ints.add(t);
								ints.add(tt);
								tags.add(word);
								tags.add(word2);
								break;
							}
						}
						tt++;
					}
				}
				t++;
			}
		}

		Stuff s = new Stuff();
		s.tags = tags;
		s.ints = ints;
		return s;
	}

	public static void writeExamples(HashSet<String> examples, String fname) {
		String fout = fname;
		ArrayList<String> lis = new ArrayList<String>(examples);
		Random generator = new Random(); 

		PrintWriter writer;
		try {
			writer = new PrintWriter(fout, "UTF-8");

			for(int i=0; i < lis.size(); i++) {
				writer.write(lis.get(i)+"||| "+1+"\n");
				int next = -1;
				while(next < 0 || next == i) {
					next = generator.nextInt(lis.size()-1);
				}
				int idx1 = generator.nextInt(2);
				int idx2 = generator.nextInt(2);

				//System.out.println(next+" "+lis.size());
				String ex1 = lis.get(i).split("\\|\\|\\|")[idx1];
				String ex2 = lis.get(next).split("\\|\\|\\|")[idx2];

				//writer.write(ex1+"|||"+ex2+"|||0|||0\n");
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static class Stuff {
		ArrayList<String> tags;
		ArrayList<Integer> ints;
	}
}
