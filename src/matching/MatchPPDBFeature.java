package matching;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import extraction.ExplorePPDB.Stuff;
import java.util.Arrays;

public class MatchPPDBFeature {

	public static void main(String[] args) {
		String f = "/Users/Wieting/Desktop/ppdb/ppdb_s_scored"; //ppdb file
		String data = "/Users/Wieting/Desktop/MTurk/ppdb/out.txt";
		
		if(args.length > 0) {
			f = args[0];
			data = args[1];
		}
		
		HashMap<String,String[]> map = getPhraseMap(f);
		ArrayList<String> lis = getData(data);
		for(String s: lis) {
			//System.out.println(s+"\t"+Arrays.toString(map.get(s)));
            if(map.containsKey(s)) {
                String[] ss = map.get(s);
                String[] arr = ss[4].split("\\s+");
                int idx = 1;
                String line = "";
                for(String s2 : arr) {
                	if(s2.trim().length() > 0) {
                		double num = Double.parseDouble(s2.split("=")[1]);
                		line = line + idx+":"+num+" ";
                		idx++;
                	}
                }
                
                if(line.length() > 0) {
                	System.out.println(line);
                }
            }
            else {
                System.out.println("None");
            }
		}
	}
	
	public static ArrayList<String> getData(String f) {
		
		ArrayList<String> lis = new ArrayList<String>();
		
		try {
			File file = new File(f);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			System.out.println("Extracting...");
			int count=0;
			while ((line = bufferedReader.readLine()) != null) {
				count++;
				String[] arr = line.split("\\|\\|\\|");
				//System.out.println(arr[arr.length - 1]);
				String p1 = arr[0].trim();
				String p2 = arr[1].trim();

				String key = p1+"|||"+p2;
				lis.add(key);
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return lis;
	}

	public static HashMap<String,String[]> getPhraseMap(String f) {

		HashSet<Stuff> examples_stuff = new HashSet<Stuff>();
		HashSet<String> done = new HashSet<String>();

		HashMap<String,String[]> map = new HashMap<String,String[]>();

		try {
			File file = new File(f);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			System.out.println("Extracting...");
			int count=0;
			while ((line = bufferedReader.readLine()) != null) {
				count++;
				String[] arr = line.split("\\|\\|\\|");
				//System.out.println(arr[arr.length - 1]);
				String p1 = arr[2].trim();
				String p2 = arr[3].trim();
				//double score = Double.parseDouble(arr[0]);
				
                
				p1=p1.replace("\"", "");
				p2=p2.replace("\"", "");
				p1=p1.replace("\'il", "\'ll");
				p2=p2.replace("\'il", "\'ll");
				String key = p1+"|||"+p2;
				map.put(key, arr);
				
				if(count==10000000)
					break;
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
}
