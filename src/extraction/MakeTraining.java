package extraction;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import extraction.ExplorePPDB.Stuff;

public class MakeTraining {

	public static void main(String[] args) {
		ArrayList<Stuff> s = ExplorePPDB.extractStuff(ExplorePPDB.getVocab());
		System.out.println(s.size());
		
		ArrayList<Stuff> training = extractNoFilter(s, 100000);
		ArrayList<Stuff> training2 = extractTTIFilter(s, 100000);
		
		for(int i=0; i < training2.size(); i++) {
			System.out.println(training2.get(i).p1+" "+training2.get(i).p2);
		}
		
		writeExamples(training2, "filtered_training_100k.txt");
		writeExamples(training, "nonfiltered_training_100k.txt");
	}

	private static ArrayList<Stuff> extractNoFilter(ArrayList<Stuff> lis, int n) {
		ArrayList<Stuff> ex = new ArrayList<Stuff>();
		for(int i=0; i < n; i++) {
			ex.add(lis.get(i));
		}
		return ex;
	}
	
	private static ArrayList<Stuff> extractTTIFilter(ArrayList<Stuff> lis, int n) {
		ArrayList<Stuff> ex = new ArrayList<Stuff>();
		int ct = 0;
		int i=0;
		while(ct < n) {
			String label = lis.get(i).parse;
			if(label.equals("[X]") || label.contains("\\") || label.contains("/") || label.contains("FRAG")) {
				//System.out.println(label);
				i++;
				continue;
			}
			ex.add(lis.get(i));
			i++;
			ct++;
		}
		return ex;
	}
	
	public static void writeExamples(ArrayList<Stuff> examples, String fname) {
		String fout = fname;
		Random generator = new Random(); 

		HashSet<String> temp = new HashSet<String>();
		for(Stuff s: examples) {
			//System.out.println(s.p1+"|||"+s.p2+"|||"+s.score+"|||"+s.wordoverlap);
			temp.add(s.p1+"|||"+s.p2+"|||"+s.score);
		}
		
		ArrayList<String> lis = new ArrayList<String>(temp);
		
		PrintWriter writer;
		try {
			writer = new PrintWriter(fout, "UTF-8");

			for(int i=0; i < lis.size(); i++) {
				writer.write(lis.get(i)+"||| "+1+"\n");
				//writer.write(ex1+"|||"+ex2+"|||0|||0\n");
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
