package extraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import extraction.ExplorePPDB.Stuff;

public class MakeDataSetMT {

	
	public static void main(String[] args) {
		//total number of examples/Users/Wieting/Documents/workspace/ppdb2/src/extraction/MakeDataSet2.java
		String f = "/Users/Wieting/Desktop/ppdb/ppdb_s_scored"; //ppdb file
		String vf = "../ppdb2/vocab.skip25w5-wiki.en";
		int chunksize = 10000; //size of chunk to sample from in ppdb
		int s_size = 5; //number of phrases to sample of each size in each chunk
		int MAX = 100;
		int N = 10;
		
		if(args.length > 0) {
			f = args[0];
			vf = args[1];
			chunksize = Integer.parseInt(args[2]);
			s_size = Integer.parseInt(args[3]);
			MAX = Integer.parseInt(args[4]);
		}
		
		ArrayList<ArrayList<String>> l = MakeDataSet2.getChunks(f,chunksize);
		//for(int i=0; i < l.size(); i++) {
		//	System.out.println(i+" "+l.get(i).size());
		//}
		
		//get first 10 chunks
		ArrayList<ArrayList<String>> new_l = new ArrayList<ArrayList<String>>();
		for(int i=0; i < N; i++) {
			new_l.add(l.get(i));
		}
		
		ArrayList<ArrayList<Stuff>> l2 = MakeDataSet2.filterLists(new_l, vf);
		for(int i=0; i < l2.size(); i++) {
			System.out.println(i+" "+l2.get(i).size());
		}
		
		ArrayList<ArrayList<Stuff>> l3 = MakeDataSet2.filterWordOverlap(l2);
		for(int i=0; i < l3.size(); i++) {
			System.out.println(i+" "+l3.get(i).size());
		}
		
		ArrayList<ArrayList<Stuff>> l4 = MakeDataSet2.getUniformSample(l3, s_size);
		for(int i=0; i < l4.size(); i++) {
			System.out.println(i+" "+l4.get(i).size());
		}
		
		ArrayList<Stuff> lis = new ArrayList<Stuff>();
		for(int i=0; i < l4.size(); i++) {
			lis.addAll(l4.get(i));
		}
		
		Collections.shuffle(lis);
		System.out.println("List size: "+lis.size());
		HashSet<String> phrases = new HashSet<String>();
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();
		
		for(int i=0; i < lis.size(); i++ ) {
			Stuff ss = lis.get(i);
			String p1 = ss.p1;
			String p2 = ss.p2;
			
			if(!phrases.contains(p1) && !phrases.contains(p2)) {
				newlis.add(ss);
				phrases.add(p1);
				phrases.add(p2);
			}
			
			if(newlis.size()==MAX)
				break;
		}
		
		System.out.println("Size after pruning: "+newlis.size());
		for(Stuff ss: newlis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			System.out.println(ss.p1+"|||"+ss.p2);
		}
	}
	
}
