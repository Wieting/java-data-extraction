package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TrimPPDB {

	public static void main(String[] args) {
		String f1 = "/shared/shelley/wieting2/ppdb/ppdb_xl_scored";
		String f2 = "/shared/shelley/wieting2/ppdb/ppdb_xxl_scored";
		
		printDiff(f1,f2);
	}

	public static void printDiff(String f1, String f2) {

		File file1 = new File(f1);
		FileReader fileReader1 = null;

		File file2 = new File(f2);
		FileReader fileReader2 = null;
		try {
			fileReader1 = new FileReader(file1);    
			fileReader2 = new FileReader(file2);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
		BufferedReader bufferedReader2 = new BufferedReader(fileReader2);

		String line1;
		String line2;
		int count = 0;
		//ArrayList<String> diff = new ArrayList<String>();
		try {
			while(true) {
				line1 = bufferedReader1.readLine();
				line2 = bufferedReader2.readLine();

				if(line1 != null && !line1.equals(line2)) {
					System.out.println("Error");
				}
				if(line1!=null) {
					count++;
				}
				else {
					if(line2==null)
						break;
					//diff.add(line2);
					System.out.println(line2);
				}
			}

			bufferedReader1.close();
			bufferedReader2.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//System.out.println(count);
		//System.out.println(diff.size());
	}
}
