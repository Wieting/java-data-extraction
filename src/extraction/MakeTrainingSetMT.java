package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import makeData.LD;
import extraction.ExplorePPDB.Stuff;

public class MakeTrainingSetMT {

	public static void main(String[] args) {
		//total number of examples/Users/Wieting/Documents/workspace/ppdb2/src/extraction/MakeDataSet2.java
		String f = "/Users/Wieting/Desktop/ppdb/ppdb_s_scored"; //ppdb file
		String vf = "../ppdb2/vocab.skip25w5-wiki.en";
		int chunksize = 10000; //size of chunk to sample from in ppdb
		int s_size = 500; //number of phrases to sample of each size in each chunk
		int MAX = 10000;

		if(args.length > 0) {
			f = args[0];
			vf = args[1];
			chunksize = Integer.parseInt(args[2]);
			s_size = Integer.parseInt(args[3]);
			MAX = Integer.parseInt(args[4]);
		}

		ArrayList<String> l = getData(f);

		ArrayList<Stuff> l2 = filterLists(l, vf);

		ArrayList<Stuff> l3 = filterWordOverlap(l2);

		ArrayList<Stuff> lis = getUniformSample(l3, s_size);

		Collections.shuffle(lis);
		System.out.println("List size: "+lis.size());
		HashSet<String> phrases = new HashSet<String>();
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();

		//remove those phrases that were in test.
		String testf = "test_set.txt";
		lis=filterPhrasesInTest(testf,lis);
		System.out.println("List size after test set filtering: "+lis.size());
		
		for(int i=0; i < lis.size(); i++ ) {
			Stuff ss = lis.get(i);
			String p1 = ss.p1;
			String p2 = ss.p2;

			if(!phrases.contains(p1) && !phrases.contains(p2)) {
				newlis.add(ss);
				phrases.add(p1);
				phrases.add(p2);
			}

			if(newlis.size()==MAX)
				break;
		}

		System.out.println("Size after pruning: "+newlis.size());
		for(Stuff ss: newlis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			System.out.println(ss.p1+"|||"+ss.p2);
		}
	}

	public static ArrayList<Stuff> filterPhrasesInTest(String testf, ArrayList<Stuff> lis) {
		HashSet<String> set = new HashSet<String>();

		try {
			File file = new File(testf);
			FileReader fileReader = new FileReader(file);
			@SuppressWarnings("resource")
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int count = 0;
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				set.add(line.trim());
				count ++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();
		
		for(Stuff ss: lis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			String t1 = ss.p1+"|||"+ss.p2;
			String t2 = ss.p2+"|||"+ss.p1;
			if(set.contains(t1) || set.contains(t2)) {
				continue;
			}
			else {
				newlis.add(ss);
			}
		}
		
		return newlis;
	}

	public static ArrayList<String> getData(String fname) {

		ArrayList<String> lis = new ArrayList<String>();

		try {
			File file = new File(fname);
			FileReader fileReader = new FileReader(file);
			@SuppressWarnings("resource")
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int count = 0;
			String line;
			System.out.println("Extracting...");
			while ((line = bufferedReader.readLine()) != null) {
				lis.add(line);
				count ++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		return lis;
	}

	public static ArrayList<Stuff> filterLists(ArrayList<String> lis, String vf) {

		HashSet<String> vocab = ExplorePPDB.getVocab(vf);
		ArrayList<ArrayList<Stuff>> newlis = new ArrayList<ArrayList<Stuff>>();

		HashSet<Stuff> examples_stuff = new HashSet<Stuff>();
		HashSet<String> done = new HashSet<String>();
		for(int i=0; i < lis.size(); i++) {
			String line = lis.get(i);
			String[] arr = line.split("\\|\\|\\|");
			String p1 = arr[2].trim();
			String p2 = arr[3].trim();
			double score = Double.parseDouble(arr[0]);
			if(LD.ld(p1,p2)<=1)
				continue;
			String[] phrase1 = p1.trim().split("\\s");
			String[] phrase2 = p2.trim().split("\\s");
			
			boolean invocab = true;
			for(String s: phrase1) {
				if(!vocab.contains(s)) {
					invocab = false;
				}
			}
			for(String s: phrase2) {
				if(!vocab.contains(s)) {
					invocab = false;
				}
			}
			if(!invocab)
				continue;

			Stuff s = new Stuff();
			s.p1=p1.trim();
			s.p2=p2.trim();
			s.score=score;
			s.wordoverlap = ExplorePPDB.getOverlapScore(phrase1,phrase2);
			s.parse = arr[1].trim();

			if(!done.contains(p1+"|||"+p2) && !done.contains(p2+"|||"+p1)) {
				done.add(p1+"|||"+p2);
				examples_stuff.add(s);
			}
		}
		ArrayList<Stuff> temp = new ArrayList<Stuff>(examples_stuff);
		Collections.sort(temp);
		return temp;
	}

	public static ArrayList<Stuff> filterWordOverlap(ArrayList<Stuff> lis) {
		//take 0.5 and lower, sort by length. Take top 100k and randomly sample 5000 no phrases repeated.
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();

		for(int j=0; j < lis.size(); j++) {
			if(lis.get(j).wordoverlap < 0.5) {
				newlis.add(lis.get(j));
			}
		}

		return newlis;
	}

	public static ArrayList<Stuff> getUniformSample(
			ArrayList<Stuff> lis, int s_size) {

		ArrayList<Stuff> newlis = new ArrayList<Stuff>();

		ArrayList<ArrayList<Stuff>> temp = new ArrayList<ArrayList<Stuff>>();
		temp.add(new ArrayList<Stuff>());
		temp.add(new ArrayList<Stuff>());
		temp.add(new ArrayList<Stuff>());

		for(int j=0; j < lis.size(); j++) {
			Stuff ss = lis.get(j);
			int l = MakeDataSet.getModifiedSize(ss.p1);
			int k = MakeDataSet.getModifiedSize(ss.p2);
			int m = k > l ? k : l;
			ss.wordoverlap = m;

			if(l < 2 || k < 2) {
				continue;
			}
			
			if(m==3) {
				temp.get(0).add(ss);
			}else if(m==4) {
				temp.get(1).add(ss);
			}else if(m>=5) {
				temp.get(2).add(ss);
			}
		}

		Collections.shuffle(temp.get(0));
		Collections.shuffle(temp.get(1));
		Collections.shuffle(temp.get(2));

		System.out.println(temp.get(0).size()+" "+temp.get(1).size()+" "+temp.get(2).size());

		newlis.addAll(MakeDataSet2.getFirstK(temp.get(0),s_size));
		newlis.addAll(MakeDataSet2.getFirstK(temp.get(1),s_size));
		newlis.addAll(MakeDataSet2.getFirstK(temp.get(2),s_size));

		return newlis;
	}
}
