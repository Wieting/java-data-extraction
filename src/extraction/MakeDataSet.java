package extraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import extraction.ExplorePPDB.Stuff;

public class MakeDataSet {

	public static void main(String[] args) {
		//ArrayList<Stuff> large = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), "/shared/shelley/wieting2/ppdb/ppdb_xl_scored");
		//String f = "/shared/shelley/wieting2/ppdb/ppdb_xxl_scored";
		//String f = "/shared/shelley/wieting2/ppdb/ppdb_diff_scored";
		String f = "/shared/shelley/wieting2/ppdb/ppdb_xl_scored";
		ArrayList<Stuff> large = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), f);
		System.out.println(f);
		
		//System.out.println(getNumberOfOverlaps(large));
		
		HashMap<Double, ArrayList<Stuff>> map = new HashMap<Double,ArrayList<Stuff>>();
		
		for(int i=0; i< large.size(); i++) {
			if(map.containsKey(large.get(i).wordoverlap)) {
				ArrayList<Stuff> temp = map.get(large.get(i).wordoverlap);
				temp.add(large.get(i));
				map.put(large.get(i).wordoverlap, temp);
			}
			else {
				ArrayList<Stuff> temp = new ArrayList<Stuff>();
				temp.add(large.get(i));
				map.put(large.get(i).wordoverlap, temp);
			}
		}
		
		//Double[] arr = map.keySet().toArray(new Double[0]);
		//for(int i=0; i < arr.length; i++) {
		//	System.out.println(arr[i]+" "+map.get(arr[i]).size());
		//}
		
		//take 0.5 and lower, sort by length. Take top 100k and randomly sample 5000 no phrases repeated.
		Double[] arr = map.keySet().toArray(new Double[0]);
		ArrayList<Stuff> lis = new ArrayList<Stuff>();
		for(int i=0; i < arr.length; i++) {
			if(arr[i] < 0.5) {
				lis.addAll(map.get(arr[i]));
			}
		}
		
		lis = getUniformSampleOfSize(lis);
		
//		//lis = sortBySize(lis);
//		lis = filterBySizeAndRandomize(lis);
//		
		HashSet<String> phrases = new HashSet<String>();
		
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();
		
		//for(int i=lis.size()-1; i>=0; i--) {
		for(int i=0; i < lis.size(); i++ ) {
			Stuff ss = lis.get(i);
			String p1 = ss.p1;
			String p2 = ss.p2;
			
			if(!phrases.contains(p1) && !phrases.contains(p2)) {
				newlis.add(ss);
				phrases.add(p1);
				phrases.add(p2);
			}
			
			if(newlis.size() == 5000)
				break;
		}
		
		for(Stuff ss: newlis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			//System.out.println(ss.p1+"|||"+ss.p2);
		}
	}

	public static ArrayList<Stuff> sortBySize(ArrayList<Stuff> lis) {
		ArrayList<Stuff> temp = new ArrayList<Stuff>();
		for(int i=0; i < lis.size(); i++) {
			Stuff ss = lis.get(i);
			int j = ss.p1.split("\\s+").length + ss.p2.split("\\s+").length;
			ss.wordoverlap=j;
			temp.add(ss);
		}
		Collections.sort(temp);
		return temp;
	}

	public static ArrayList<Stuff> getUniformSampleOfSize(ArrayList<Stuff> lis) {
		ArrayList<ArrayList<Stuff>> temp = new ArrayList<ArrayList<Stuff>>();
		temp.add(new ArrayList<Stuff>());
		temp.add(new ArrayList<Stuff>());
		temp.add(new ArrayList<Stuff>());
		temp.add(new ArrayList<Stuff>());
		for(int i=0; i < lis.size(); i++) {
			Stuff ss = lis.get(i);
			int j = getModifiedSize(ss.p1);
			int k = getModifiedSize(ss.p2);
			int m = k > j ? k : j;
			ss.wordoverlap = m;
			if(m==3) {
				temp.get(0).add(ss);
			}else if(m==4) {
				temp.get(1).add(ss);
			}else if(m==5) {
				temp.get(2).add(ss);
			}else if(m >=6) {
				temp.get(3).add(ss);
			}
		}
		
		Collections.shuffle(temp.get(0));
		Collections.shuffle(temp.get(1));
		Collections.shuffle(temp.get(2));
		Collections.shuffle(temp.get(3));
		
        System.out.println(temp.get(0).size());
        System.out.println(temp.get(1).size());
        System.out.println(temp.get(2).size());
        System.out.println(temp.get(3).size());
        
		//grab 2000 from each
		ArrayList<Stuff> r = new ArrayList<Stuff>();
		for (int i=0; i < 2000; i++) {
			r.add(temp.get(0).get(i));
			r.add(temp.get(1).get(i));
			r.add(temp.get(2).get(i));
			r.add(temp.get(3).get(i));
			
			if(i >= temp.get(0).size())
				break;
			if(i >= temp.get(1).size())
				break;
			if(i >= temp.get(2).size())
				break;
			if(i >= temp.get(3).size())
				break;
		}
		
		return r;
	}
	
	public static ArrayList<Stuff> filterBySizeAndRandomize(ArrayList<Stuff> lis) {
		ArrayList<Stuff> temp = new ArrayList<Stuff>();
		for(int i=0; i < lis.size(); i++) {
			Stuff ss = lis.get(i);
			int j = getModifiedSize(ss.p1);
			int k = getModifiedSize(ss.p2);
			if(j >= 3 || k >= 3)
				temp.add(ss);
		}
		Collections.shuffle(temp);
		return temp;
	}
	
	public static int getModifiedSize(String s) {
		String[] arr = s.split("\\s+");
		int ct = 0;
		for(int i=0; i < arr.length; i++) {
			if(arr[i].length() > 1)
				ct++;
		}
		
		return ct;
	}
	
	public static ArrayList<Stuff> getNoOverlap(ArrayList<Stuff> lis) {
		ArrayList<Stuff> temp = new ArrayList<Stuff>();
		
		for(Stuff ss: lis) {
			if(ss.wordoverlap < 0.1) {
				temp.add(ss);
			}
		}
		
		return temp;
	}
	
	public static ArrayList<Stuff> getDiff(ArrayList<Stuff> lis1, ArrayList<Stuff> lis2) {
		ArrayList<Stuff> diff = new ArrayList<Stuff>();
		for(int i=0; i< lis1.size(); i++) {
			for(int j=0; j < lis2.size(); j++) {
				
			}
		}
		
		return diff;
	}

	public static ArrayList<Stuff> getNegatives(ArrayList<Stuff> s, int nsamples) {
		ArrayList<Stuff> temp = new ArrayList<Stuff>();

		HashSet<Integer> done = new HashSet<Integer>();

		for(int i=0; i < nsamples; i++) {
			int idx = 7;
		}
		return temp;
	}

	public static int randInt(int min, int max) {

		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}
	
	public class Pair {
		
		double value;
		double percentage;
	}
	
	public static int getNumberOfOverlaps(ArrayList<Stuff> ss) {
		HashSet<Double> set = new HashSet<Double>();
		
		for(int i=0; i < ss.size(); i++) {
			set.add(ss.get(i).wordoverlap);
		}
		return set.size();
	}
	
	//ArrayList<Stuff> xlarge = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), "/shared/shelley/wieting2/ppdb_m_scored");

	//ArrayList<Stuff> diff = getDiff(large,xlarge);
	
	//LD distance, vocab, now extract those with zero overlap and sort by score (number of tokens (of size > 1)
	//ArrayList<Stuff> newlis = getNoOverlap(large);
	//System.out.println("Sorting");
	//large = sortBySize(large);
	
	//int count = 0;
	//for(int i=large.size()-1; i >= 0; i--) {
	//	System.out.println(large.get(i).p1+"|||"+large.get(i).p2);
	//	count++;
	//	if(count > 5000)
	//		break;
	//}
}
