package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import makeData.LD;
import extraction.ExplorePPDB.Stuff;

public class MakeDataSet2 {

	public static void main(String[] args) {
		//total number of examples/Users/Wieting/Documents/workspace/ppdb2/src/extraction/MakeDataSet2.java
		String f = "/Users/Wieting/Desktop/ppdb/ppdb_s_scored"; //ppdb file
		String vf = "../ppdb2/vocab.skip25w5-wiki.en";
		int chunksize = 100000; //size of chunk to sample from in ppdb
		int s_size = 100; //number of phrases to sample of each size in each chunk
		int MAX = 100;
		
		if(args.length > 0) {
			f = args[0];
			vf = args[1];
			chunksize = Integer.parseInt(args[2]);
			s_size = Integer.parseInt(args[3]);
			MAX = Integer.parseInt(args[4]);
		}
		
		ArrayList<ArrayList<String>> l = getChunks(f,chunksize);
		for(int i=0; i < l.size(); i++) {
			System.out.println(i+" "+l.get(i).size());
		}
		
		ArrayList<ArrayList<Stuff>> l2 = filterLists(l, vf);
		for(int i=0; i < l2.size(); i++) {
			System.out.println(i+" "+l2.get(i).size());
		}
		
		ArrayList<ArrayList<Stuff>> l3 = filterWordOverlap(l2);
		for(int i=0; i < l3.size(); i++) {
			System.out.println(i+" "+l3.get(i).size());
		}
		
		ArrayList<ArrayList<Stuff>> l4 = getUniformSample(l3, s_size);
		for(int i=0; i < l4.size(); i++) {
			System.out.println(i+" "+l4.get(i).size());
		}
		
        ArrayList<Stuff> lis = new ArrayList<Stuff>();
		for(int i=0; i < l4.size(); i++) {
			lis.addAll(l4.get(i));
		}
		
		Collections.shuffle(lis);
		HashSet<String> phrases = new HashSet<String>();
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();
		
		for(int i=0; i < lis.size(); i++ ) {
			Stuff ss = lis.get(i);
			String p1 = ss.p1;
			String p2 = ss.p2;
			
			//if(!phrases.contains(p1) && !phrases.contains(p2)) {
				newlis.add(ss);
				phrases.add(p1);
				phrases.add(p2);
			//}
			
			if(newlis.size()==MAX)
				break;
		}
		
		for(Stuff ss: newlis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			System.out.println(ss.p1+"|||"+ss.p2);
		}
	}

	public static ArrayList<ArrayList<Stuff>> getUniformSample(
			ArrayList<ArrayList<Stuff>> lis, int s_size) {

		ArrayList<ArrayList<Stuff>> newlis = new ArrayList<ArrayList<Stuff>>();
		
		for(int i=0; i < lis.size(); i++) {
			ArrayList<ArrayList<Stuff>> temp = new ArrayList<ArrayList<Stuff>>();
			temp.add(new ArrayList<Stuff>());
			temp.add(new ArrayList<Stuff>());
			temp.add(new ArrayList<Stuff>());

			for(int j=0; j < lis.get(i).size(); j++) {
				Stuff ss = lis.get(i).get(j);
				int l = MakeDataSet.getModifiedSize(ss.p1);
				int k = MakeDataSet.getModifiedSize(ss.p2);
				int m = k > l ? k : l;
				ss.wordoverlap = m;

				if(m==3) {
					temp.get(0).add(ss);
				}else if(m==4) {
					temp.get(1).add(ss);
				}else if(m>=5) {
					temp.get(2).add(ss);
				}
			}
			
			Collections.shuffle(temp.get(0));
			Collections.shuffle(temp.get(1));
			Collections.shuffle(temp.get(2));
			
			System.out.println(temp.get(0).size()+" "+temp.get(1).size()+" "+temp.get(2).size());
			
			ArrayList<Stuff> tt = new ArrayList<Stuff>();
			tt.addAll(getFirstK(temp.get(0),s_size));
			tt.addAll(getFirstK(temp.get(1),s_size));
			tt.addAll(getFirstK(temp.get(2),s_size));

			newlis.add(tt);
		}        
        return newlis;
	}

	public static ArrayList<Stuff> getFirstK(ArrayList<Stuff> lis, int num) {
		ArrayList<Stuff> newlis = new ArrayList<Stuff>();
		num = num < lis.size() ? num : lis.size();
		
		for(int i=0; i < num; i++) {
			newlis.add(lis.get(i));
		}
		
		return newlis;
	}
	
	public static ArrayList<ArrayList<Stuff>> filterWordOverlap(ArrayList<ArrayList<Stuff>> lis) {
		//take 0.5 and lower, sort by length. Take top 100k and randomly sample 5000 no phrases repeated.
		ArrayList<ArrayList<Stuff>> newlis = new ArrayList<ArrayList<Stuff>>();
		
		for(int i=0; i < lis.size(); i++) {
			ArrayList<Stuff> curr = new ArrayList<Stuff>();
			for(int j=0; j < lis.get(i).size(); j++) {
				if(lis.get(i).get(j).wordoverlap < 0.5) {
					curr.add(lis.get(i).get(j));
				}
			}
			newlis.add(curr);
		}
		
		return newlis;
	}
	
	public static ArrayList<ArrayList<String>> getChunks(String fname, int chunksize) {
		
		ArrayList<ArrayList<String>> lis = new ArrayList<ArrayList<String>>();
		
		try {
		File file = new File(fname);
		FileReader fileReader = new FileReader(file);
		@SuppressWarnings("resource")
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		int count = 0;
		String line;
        System.out.println("Extracting...");
		ArrayList<String> curr = new ArrayList<String>();
        while ((line = bufferedReader.readLine()) != null) {
        	if(curr.size() == chunksize) {
				lis.add(curr);
				curr = new ArrayList<String>();
			}
			curr.add(line);
			count ++;
		}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return lis;
	}
	
	public static ArrayList<ArrayList<Stuff>> filterLists(ArrayList<ArrayList<String>> lis, String vf) {
		
		HashSet<String> vocab = ExplorePPDB.getVocab(vf);
		ArrayList<ArrayList<Stuff>> newlis = new ArrayList<ArrayList<Stuff>>();
		
		for(int i=0; i < lis.size(); i++) {
			HashSet<Stuff> examples_stuff = new HashSet<Stuff>();
			HashSet<String> done = new HashSet<String>();
			for(int j=0; j < lis.get(i).size(); j++) {
				String line = lis.get(i).get(j);
				String[] arr = line.split("\\|\\|\\|");
				String p1 = arr[2];
				String p2 = arr[3];
				double score = Double.parseDouble(arr[0]);
				if(LD.ld(p1,p2)<=1)
					continue;
				String[] phrase1 = p1.trim().split("\\s");
				String[] phrase2 = p2.trim().split("\\s");
				
				boolean invocab = true;
				for(String s: phrase1) {
					if(!vocab.contains(s)) {
						invocab = false;
					}
				}
				for(String s: phrase2) {
					if(!vocab.contains(s)) {
						invocab = false;
					}
				}
				if(!invocab)
					continue;
				
				Stuff s = new Stuff();
				s.p1=p1.trim();
				s.p2=p2.trim();
				s.score=score;
				s.wordoverlap = ExplorePPDB.getOverlapScore(phrase1,phrase2);
				s.parse = arr[1].trim();
				
				if(!done.contains(p1+"|||"+p2) && !done.contains(p2+"|||"+p1)) {
					done.add(p1+"|||"+p2);
					examples_stuff.add(s);
				}
			}
			ArrayList<Stuff> temp = new ArrayList<Stuff>(examples_stuff);
			Collections.sort(temp);
			newlis.add(temp);
		}
		
		return newlis;
	}
	
	public static void extract() {
		//ArrayList<Stuff> large = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), "/shared/shelley/wieting2/ppdb/ppdb_xl_scored");
		//String f = "/shared/shelley/wieting2/ppdb/ppdb_xxl_scored";
		//String f = "/shared/shelley/wieting2/ppdb/ppdb_diff_scored";
		String f = "/shared/shelley/wieting2/ppdb/ppdb_xl_scored";
		ArrayList<Stuff> large = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), f);
		System.out.println(f);

		//System.out.println(getNumberOfOverlaps(large));

		HashMap<Double, ArrayList<Stuff>> map = new HashMap<Double,ArrayList<Stuff>>();

		for(int i=0; i< large.size(); i++) {
			if(map.containsKey(large.get(i).wordoverlap)) {
				ArrayList<Stuff> temp = map.get(large.get(i).wordoverlap);
				temp.add(large.get(i));
				map.put(large.get(i).wordoverlap, temp);
			}
			else {
				ArrayList<Stuff> temp = new ArrayList<Stuff>();
				temp.add(large.get(i));
				map.put(large.get(i).wordoverlap, temp);
			}
		}

		//Double[] arr = map.keySet().toArray(new Double[0]);
		//for(int i=0; i < arr.length; i++) {
		//	System.out.println(arr[i]+" "+map.get(arr[i]).size());
		//}

		//take 0.5 and lower, sort by length. Take top 100k and randomly sample 5000 no phrases repeated.
		Double[] arr = map.keySet().toArray(new Double[0]);
		ArrayList<Stuff> lis = new ArrayList<Stuff>();
		for(int i=0; i < arr.length; i++) {
			if(arr[i] < 0.5) {
				lis.addAll(map.get(arr[i]));
			}
		}

		lis = MakeDataSet.getUniformSampleOfSize(lis);

		//		//lis = sortBySize(lis);
		//		lis = filterBySizeAndRandomize(lis);
		//
		HashSet<String> phrases = new HashSet<String>();

		ArrayList<Stuff> newlis = new ArrayList<Stuff>();

		//for(int i=lis.size()-1; i>=0; i--) {
		for(int i=0; i < lis.size(); i++ ) {
			Stuff ss = lis.get(i);
			String p1 = ss.p1;
			String p2 = ss.p2;

			if(!phrases.contains(p1) && !phrases.contains(p2)) {
				newlis.add(ss);
				phrases.add(p1);
				phrases.add(p2);
			}

			if(newlis.size() == 5000)
				break;
		}

		for(Stuff ss: newlis) {
			ss.p1=ss.p1.replace("\"", "");
			ss.p2=ss.p2.replace("\"", "");
			ss.p1=ss.p1.replace("\'il", "\'ll");
			ss.p2=ss.p2.replace("\'il", "\'ll");
			//System.out.println(ss.p1+"|||"+ss.p2);
		}
	}
}
