package extraction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import extraction.ExplorePPDB.Stuff;

public class ExplorePPDBPOS {

	public static void main(String[] args) {
		ArrayList<Stuff> large = ExplorePPDB.extractStuff(ExplorePPDB.getVocab(), "/shared/shelley/wieting2/matlab/paraphrase_RNN/bigrams/make_data/ppdb/ppdb-1.0-l-phrasal_scored");

		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		HashMap<String, Integer> map = new HashMap<String, Integer>();

		for(Stuff s: large) {
			String p1 = getPOSString(pipeline,s.p1);
			String p2 = getPOSString(pipeline, s.p2);
			String pp = p1+" | "+p2;

			if(map.containsKey(pp)) {
				map.put(pp, map.get(pp)+1);
			}
			else {
				map.put(pp,1);
			}	
		}

		System.out.println(map.size());
		double total=0;
		double in = 0;
		int count =0;
		Map<String,Integer> m = sortByValues(map);
		for(String s: m.keySet()) {
			if(m.get(s) > 100) {
				System.out.println(s+" "+m.get(s));
				count++;
				in += m.get(s);
			}
			total += m.get(s);
		}
		System.out.println(count);
		System.out.println(map.size());
		System.out.println(in/total);
	}

	public static String getPOSString(StanfordCoreNLP pipeline, String ss) {
		Annotation document = new Annotation(ss);
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		String st = "";

		for(CoreMap sentence: sentences) {

			List<CoreLabel> lis = sentence.get(TokensAnnotation.class);
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				String pos = token.get(PartOfSpeechAnnotation.class);
				if(!pos.equals(",") && !pos.equals(".") && !pos.equals(":")) {
					st = st+changePOS(pos)+" ";
				}
			}
		}

		st=st.trim();
		return st;
	}

	public static String changePOS(String s) {
		if(s.startsWith("V"))
			return "V";
		if(s.startsWith("J"))
			return "J";
		if(s.startsWith("N"))
			return "N";
		if(s.startsWith("R"))
			return "R";
		return s;
	}

	

	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map){
		List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		//LinkedHashMap will keep the keys in the order they are inserted
		//which is currently sorted on natural ordering
		Map<K,V> sortedMap = new LinkedHashMap<K,V>();

		for(Map.Entry<K,V> entry: entries){
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}
}