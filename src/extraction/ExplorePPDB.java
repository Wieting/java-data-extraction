package extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import makeData.LD;
import makeData.makeData2.Stuff;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class ExplorePPDB {

	public static void main(String[] args) {
		explore(extractStuff(getVocab()));
	}

	public static HashSet<String> getVocab(String fname) {
		HashSet<String> vocab = new HashSet<String>();
		File vfile = new File(fname);
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(vfile);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		String line;
		int count = 0;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				vocab.add(line.trim());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return vocab;
	}
	
	public static HashSet<String> getVocab() {
		return getVocab("../ppdb2/vocab.skip25w5-wiki.en");
	}
	
	public static ArrayList<Stuff> extractStuff(HashSet<String> vocab) {
		String fname= "/shared/shelley/wieting2/matlab/paraphrase_RNN/bigrams/make_data/ppdb/ppdb-1.0-l-phrasal_scored";
		return extractStuff(vocab,fname);
	}
	
	public static ArrayList<Stuff> extractStuff(HashSet<String> vocab, String fname) {
		HashSet<Stuff> examples_stuff = new HashSet<Stuff>();
		HashSet<String> done = new HashSet<String>();
		
		try {
			File file = new File(fname);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int count = 0;
			String line;
            System.out.println("Extracting...");
			while ((line = bufferedReader.readLine()) != null) {
				count ++;
				if(count % 10000 == 0) {
					//System.out.println("read "+count+" lines and found "+examples_stuff.size() +" examples.");
					//break;
				}
				String[] arr = line.split("\\|\\|\\|");
				//System.out.println(arr[arr.length - 1]);
				String p1 = arr[2];
				String p2 = arr[3];
				double score = Double.parseDouble(arr[0]);

				if(LD.ld(p1,p2)<=1)
					continue;

				String[] phrase1 = p1.trim().split("\\s");
				String[] phrase2 = p2.trim().split("\\s");
				
				boolean invocab = true;
				for(String s: phrase1) {
					if(!vocab.contains(s)) {
						invocab = false;
					}
				}
				
				for(String s: phrase2) {
					if(!vocab.contains(s)) {
						invocab = false;
					}
				}
				
				if(!invocab)
					continue;
				
				Stuff s = new Stuff();
				s.p1=p1.trim();
				s.p2=p2.trim();
				s.score=score;
				s.wordoverlap = getOverlapScore(phrase1,phrase2);
				s.parse = arr[1].trim();
				
				if(!done.contains(p1+"|||"+p2) && !done.contains(p2+"|||"+p1)) {
					done.add(p1+"|||"+p2);
					examples_stuff.add(s);
				}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ArrayList<Stuff> lis = new ArrayList<Stuff>(examples_stuff);
		Collections.sort(lis);
		return lis;
	}

	
	public static void explore(ArrayList<Stuff> lis) {
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		int ct = 0;
		HashSet<String> examples = new HashSet<String>();
		for(Stuff s: lis) {
			if(!s.parse.equals("[VP]"))// || !s.parse.equals("[VP]")  || !s.parse.equals("[ADVP]") || !s.parse.equals("[NP]"))
				continue;
			System.out.println(s.p1+"|||"+s.p2+"|||"+s.score+"|||"+s.wordoverlap);
			examples.add(s.p1+"|||"+s.p2+"|||"+s.score);
			ct++;
			if(map.containsKey(s.parse)) {
				map.put(s.parse, map.get(s.parse)+1);
			}
			else {
				map.put(s.parse,1);
			}	
			//if(ct >= 300000)
			//	break;
		}

		System.out.println(lis.size());
		//for(String s: map.keySet()) {
		//	if(!s.contains("/") && !s.contains("\\")) {
		//			System.out.println(s+" "+map.get(s));
		//	}
		//}
	}

	public static void writeExamples(HashSet<String> examples, String fname) {
		String fout = fname;
		ArrayList<String> lis = new ArrayList<String>(examples);
		Random generator = new Random(); 

		PrintWriter writer;
		try {
			writer = new PrintWriter(fout, "UTF-8");

			for(int i=0; i < lis.size(); i++) {
				writer.write(lis.get(i)+"||| "+1+"\n");
				int next = -1;
				while(next < 0 || next == i) {
					next = generator.nextInt(lis.size()-1);
				}
				int idx1 = generator.nextInt(2);
				int idx2 = generator.nextInt(2);

				//System.out.println(next+" "+lis.size());
				String ex1 = lis.get(i).split("\\|\\|\\|")[idx1];
				String ex2 = lis.get(next).split("\\|\\|\\|")[idx2];

				//writer.write(ex1+"|||"+ex2+"|||0|||0\n");
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static double getOverlapScore(String[] arr1, String[] arr2) {
		String[] base = arr1;
		String[] other = arr2;
		if(base.length > arr2.length) {
			base = arr2;
			other = arr1;
		}
		
		double total = base.length;
		
		int overlap = 0;
		for(int i=0; i < base.length; i++) {
			for(int j=0; j < other.length; j++) {
				if(LD.ld(base[i],other[j])<=1) {
					overlap += 1;
					break;
				}
			}
		}
		
		return overlap/total;		
	}
	
	public static class Stuff implements Comparable {
		String p1;
		String p2;
		double score;
		double wordoverlap;
		String parse;
		
		@Override
		public int compareTo(Object o) {
			// TODO Auto-generated method stub
			Stuff s = (Stuff) o;
			if(wordoverlap > s.wordoverlap) {
				return 1;
			}
			else if(wordoverlap == s.wordoverlap) {
				return 0;
			}
			else {
				return -1;
			}
		}	
	}
}