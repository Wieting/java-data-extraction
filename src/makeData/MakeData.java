package makeData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class MakeData {

	public static void main(String[] args) {
		
		 // Initialize the tagger
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        HashSet<String> examples = new HashSet<String>();
        HashSet<String> done = new HashSet<String>();
        
        try {
			File file = new File("/shared/shelley/wieting2/matlab/paraphrase_RNN/bigrams/make_data/ppdb/ppdb-1.0-l-phrasal_scored");
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			int count = 0;
			while ((line = bufferedReader.readLine()) != null) {
				count ++;
				if(count % 10000 == 0) {
					System.out.println("read "+count+" lines and found "+examples.size() +" examples.");
					//break;
				}
				String[] arr = line.split("\\|\\|\\|");
				String p1 = arr[2];
				String p2 = arr[3];
				double score = Double.parseDouble(arr[0]);
				String[] phrase1 = p1.trim().split("\\s");
				String[] phrase2 = p2.trim().split("\\s");
				if(phrase1.length != 2 || phrase2.length != 2)
					continue;
				ArrayList<String> pos1 = getTags(p1,pipeline);
				ArrayList<String> pos2 = getTags(p2,pipeline);
				if(pos1.get(0).startsWith("J") && pos2.get(0).startsWith("J")) {
					if(pos1.get(1).startsWith("N") && pos2.get(1).startsWith("N")) {
						if(p1.contains("-lrb-") || p1.contains("-rrb-") || p2.contains("-lrb") || p2.contains("-rrb"))
							continue;
						if(p1.compareTo(p1) < 0) {
							if(!done.contains(p1+"|||"+p2)) {
								examples.add(p1+"|||"+p2+"|||"+score);
								done.add(p1+"|||"+p2);
							}
						}
						else {
							if(!done.contains(p1+"|||"+p2)) {
								examples.add(p2+"|||"+p1+"|||"+score);
								done.add(p1+"|||"+p2);
							}
						}
					}
				}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        for(String s: examples) {
        	//System.out.println(s);
        }
        
        System.out.println(examples.size());
        writeExamples(examples, args[0]);
	}

	private static ArrayList<String> getTags(String p1, StanfordCoreNLP pipeline) {
		Annotation document = new Annotation(p1);

		// run all Annotators on this text
		pipeline.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		ArrayList<String> tags = new ArrayList<String>();
		for(CoreMap sentence: sentences) {
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			List<CoreLabel> lis = sentence.get(TokensAnnotation.class);
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				String lem = token.get(LemmaAnnotation.class);
				tags.add(pos);
				//System.out.println(word+" "+lem+" "+pos);
			}
		}
		
		return tags;
	}

	public static void writeExamples(HashSet<String> examples, String fname) {
		String fout = fname;
		ArrayList<String> lis = new ArrayList<String>(examples);
		Random generator = new Random(); 

		PrintWriter writer;
		try {
			writer = new PrintWriter(fout, "UTF-8");

			for(int i=0; i < lis.size(); i++) {
				writer.write(lis.get(i)+"||| "+1+"\n");
				int next = -1;
				while(next < 0 || next == i) {
					next = generator.nextInt(lis.size()-1);
				}
				int idx1 = generator.nextInt(2);
				int idx2 = generator.nextInt(2);

				//System.out.println(next+" "+lis.size());
				String ex1 = lis.get(i).split("\\|\\|\\|")[idx1];
				String ex2 = lis.get(next).split("\\|\\|\\|")[idx2];

				writer.write(ex1+"|||"+ex2+"|||0|||0\n");
			}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

